# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 12:33:15 2017

@author: rclement
"""
import numpy as np
import pyLDM.LDMmodel as LDM     
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


#
fn = r'E:\Mynalysis\Functional rotation\input\output Griffin FEC series00050.txt'
fn = r'E:\Mynalysis\Functional rotation\input\output Griffin FEC series00070.txt'
datinp = np.genfromtxt(fn, dtype = None , delimiter=',', skip_header=1,skip_footer=100, names=['Date','time', 'msec', 'wu', 'wT', 'up', 'vp', 'wp', 'drct'])
# create model
hrng = 150
vrng = 200
mod = LDM.diffuse(1000,15,0.6,280,hrng,vrng)  


# set up plot
fig = plt.figure()
plt.ion()
ax1 = plt.subplot2grid((6, 2), (0, 0), colspan=2 , rowspan=4)
ax2 = plt.subplot2grid((6, 2), (4, 0), colspan=2 )
ax3 = plt.subplot2grid((6, 2), (5, 0), colspan=2 )

# add initial particles
flg = 1
dwu = datinp['wu'][0]
dwt = datinp['wT'][0]
dup = datinp['up'][0]
dvp = datinp['vp'][0]
dwp = datinp['wp'][0]
ddir = datinp['drct'][0] 
mod.add_particles(10,[0],[0],[30],uw=dwu,wT=dwt,up=dup, vp=dvp, wp=dwp, theta=ddir,flag=flg)

#loop over time (0.1 sec time step)
for ix in range(len(datinp)):   
    # update particles
    mod.update_particles(0.1)

    if np.mod(ix,200) == 0:
        ax1.cla()
        ax2.cla()
        ax3.cla()
        ax1.scatter(mod.x,mod.y, s=1, c=mod.z)
        ax2.scatter(mod.x,mod.z, s=1, c=mod.y)
        ax3.scatter(mod.y,mod.z, s=1, c=mod.x)
        ax1.set_xlim(-1*hrng,hrng)
        ax1.set_ylim(-1*hrng,hrng)
        ax2.set_xlim(-1*hrng,hrng)
        ax2.set_ylim(0,vrng)
        ax3.set_xlim(-1*hrng,hrng)
        ax3.set_ylim(0,vrng)
        plt.suptitle('time, sec: '+str(ix*0.1))
        plt.pause(0.01)

    # add new particles at each time step
    dwu = datinp['wu'][ix]
    dwt = datinp['wT'][ix]
    dup = datinp['up'][ix]
    dvp = datinp['vp'][ix]
    dwp = datinp['wp'][ix]
    ddir = datinp['drct'][ix] 
    mod.add_particles(10,[0],[0],[30],uw=dwu,wT=dwt,up=dup, vp=dvp, wp=dwp, theta=180,flag=flg)

xi,yi = np.mgrid[0:360:1,0:300:1]
yi = yi/3
plt.figure()
plt.contourf(xi[:,0:-1], yi[:,0:-1],  mod.exit_count[:,0:-1], 20, cmap=plt.cm.rainbow)#,
plt.show()


xi,yi = np.mgrid[0:401:1,0:401:1]
xi = (xi-200) * (2*mod.range+1)/401
yi = (yi-200) * (2*mod.range+1)/401
plt.figure()
plt.contourf(xi, yi,  mod.footprint_count, 20, cmap=plt.cm.rainbow)#,
plt.show()

