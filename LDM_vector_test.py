# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 12:33:15 2017

@author: rclement
"""
import numpy as np
import pyLDM.LDMmodel as LDM     
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# create model
hrng = 1000
vrng = 2000
mod = LDM.diffuse(1000,15,0.6,280,hrng,vrng)  


# set up plot
fig = plt.figure()
plt.ion()
ax1 = plt.subplot2grid((6, 2), (0, 0), colspan=2 , rowspan=4)
ax2 = plt.subplot2grid((6, 2), (4, 0), colspan=2 )
ax3 = plt.subplot2grid((6, 2), (5, 0), colspan=2 )

# add initial particles
flg = 1
azim = 0
elev = 20
rang = 1000
mod.add_particle_vector(20,0,0,1, azim,elev,rang,  ustar=0.3,H=200, theta=67, flag=flg)

#loop over time (0.1 sec time step)
istp = 0
for ix in np.arange(20000):   
    # update particles
    mod.update_particles(0.1)
    
#    if np.mod(ix,100) == 0:
#        # add new particles at each mod step
#        mod.add_particle_vector(20,0,0,1, azim, elev, rang,  ustar=0.3,H=200, theta=67, flag=flg)

    if np.mod(ix,100) == 0:
        istp = istp + 1
        # add new particles at each mod step
        mod.add_particle_vector(50,0,0,1, azim+ istp ,elev, rang,  ustar=0.3,H=200, theta=67, flag=flg)

    if np.mod(ix,200) == 0:
        # update plot
        ax1.cla()
        ax2.cla()
        ax3.cla()
        ax1.scatter(-1*mod.y,mod.x, s=1, c=mod.z)
        ax2.scatter(mod.x,mod.z, s=1, c=mod.y)
        ax3.scatter(-1*mod.y,mod.z, s=1, c=mod.x)
        ax1.set_xlim(-1*hrng,hrng)
        ax1.set_ylim(-1*hrng,hrng)
        ax2.set_xlim(-1*hrng,hrng)
        ax2.set_ylim(0,vrng)
        ax3.set_xlim(-1*hrng,hrng)
        ax3.set_ylim(0,vrng)
        plt.suptitle('time, sec: '+str(ix*0.1))
        plt.pause(0.01)


xi,yi = np.mgrid[0:360:1,0:300:1]
yi = yi/3
plt.figure()
plt.contourf(xi[:,0:-1], yi[:,0:-1],  mod.exit_count[:,0:-1], 20, cmap=plt.cm.rainbow)#,
plt.show()


xi,yi = np.mgrid[0:401:1,0:401:1]
xi = (xi-200) * (2*mod.range+1)/401
yi = (yi-200) * (2*mod.range+1)/401
plt.figure()
plt.contourf(-yi, xi, np.log( mod.footprint_count + 1), 20, cmap=plt.cm.rainbow)#,
plt.show()

