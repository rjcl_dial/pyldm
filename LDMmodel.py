# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 12:33:15 2017

@author: rclement
"""
import numpy as np
#=============================================================================================
class particle_detector():
    def __init__(self, x, y, z, radius):
        self.x = x
        self.y = y
        self.z = z
        self.radius = radius
        self.source_x = np.reshape(np.array([]),[0,1])
        self.source_y = np.reshape(np.array([]),[0,1])
        self.source_z = np.reshape(np.array([]),[0,1])
        self.source_H = np.reshape(np.array([]),[0,1])
        self.source_ustar = np.reshape(np.array([]),[0,1])
        
    def measure(self, x, y, z, xo, yo, zo, H, ustar):
        dist = np.sqrt((x - self.x)**2 + (y - self.y)**2  + (z - self.z)**2)
        idd = dist < self.radius
        self.source_x = np.append(self.source_x, xo[idd])
        self.source_y = np.append(self.source_y, yo[idd])
        self.source_z = np.append(self.source_z, zo[idd])
        self.source_H = np.append(self.source_H, H[idd])
        self.source_ustar = np.append(self.source_ustar, ustar[idd])
        
#=============================================================================================
class diffuse():
    def __init__(self, BL_height, disp_height, roughness, Tv, max_range, max_height):
        # sets initial connditions - assumed constant over the entire domain
        self.x = None
        self.BL_height = BL_height
        self.disp_height = disp_height
        self.Tv = Tv
        self.roughness = roughness
        self.range = max_range
        self.max_height = max_height
        self.exit_count = np.zeros([360,300])
        self.footprint_count = np.zeros([401,401])
        self.tds = 0
        self.detectors = []
        # define empty particle data arrays
        self.xo = np.reshape(np.array([]),[0,1])
        self.yo = np.reshape(np.array([]),[0,1])
        self.zo = np.reshape(np.array([]),[0,1])
        self.x = np.reshape(np.array([]),[0,1])
        self.y = np.reshape(np.array([]),[0,1])
        self.z = np.reshape(np.array([]),[0,1])
        self.up = np.reshape(np.array([]),[0,1])
        self.vp = np.reshape(np.array([]),[0,1])
        self.wp = np.reshape(np.array([]),[0,1])
        self.theta = np.reshape(np.array([]),[0,1])
        self.H = np.reshape(np.array([]),[0,1])
        self.ustar = np.reshape(np.array([]),[0,1])
        self.flag = np.reshape(np.array([]),[0,1])

    def add_detector(self, x, y, z, radius):
        adetector = particle_detector( x, y, z, radius)
        self.detectors.append(adetector)
        
    def _give_new_source_particles(self, x, y, z, count):
         # get the x,y, and z cartesian domain limits
        if len(x) == 1:
            x0 = x
            x1 = x
        else:
            x0 = x[0]
            x1 = x[1]
        if len(y) == 1:
            y0 = y
            y1 = y
        else:
            y0 = y[0]
            y1 = y[1]
        if len(z) == 1:
            z0 = z
            z1 = z
        else:
            z0 = z[0]
            z1 = z[1]
        newx = np.random.uniform(x0,x1,count)    
        newy = np.random.uniform(y0,y1,count)    
        newz = np.random.uniform(z0,z1,count)    
        return( newx, newy, newz)

        
    def _give_new_source_particle_vector(self, x, y, z, az, el, rg, count):
         # get the x,y, and z cartesian domain limits
        dx = rg*np.cos(np.deg2rad(el))*np.cos(np.deg2rad(az))
        dy = rg*np.cos(np.deg2rad(el))*np.sin(np.deg2rad(az))
        dz = rg*np.sin(np.deg2rad(el))
        if count < 0:
            newx = np.random.uniform(x,x+dx,-1*count)    
            newy = np.random.uniform(y,y+dy,-1*count)    
            newz = np.random.uniform(z,z+dz,-1*count)    
        else:
            newx = np.linspace(x,x+dx, num=count)    
            newy = np.linspace(y,y+dy, num=count)     
            newz = np.linspace(z,z+dz, num=count)   
        idbg = newz < self.roughness  
        newz[idbg] = self.roughness  
        return( newx, newy, newz)
        
    def add_particles(self, count, x, y, z, ustar=None,  uw=None,  H=None,  wT=None, theta=0, up=0, vp=0, wp=0, flag=0):
        #  This function adds particles to the set of all particles used in the diffusion calculations
        #
        #  count is the number of particles to add
        #
        #  x, y and z are lists of either lenght 1 or 2  They define the range of the source area
        #        if all three are single values then the source is a point
        #        if one variable has two values then it is a line source
        #        if two variables have two values then it is an area source
        #        if all three variables have two values then it is a volume source
        #
        #  ustar and uw are friction velocity and uw covariance.  If ustar is not given then it 
        #       will be calculated from uw
        #
        #  H and wT are sensible heat flux and wT covariance.  If H is not given then it 
        #       will be calculated from wT
        #
        #   theta is the low frequency wind direction in degrees
        #
        #   up, vp, and wp are initial values of the u',v' and w' values
        #
        #   flag is a label attached to the group of particles created.
        #

        newx, newy, newz = self._give_new_source_particles( x, y, z,count)
        
        # convert low freq wind direction to radians
        theta = np.deg2rad(theta)
        
        # get ustar and w'T' values
        tmp_H    =  H/1200 if H     is not None else 100/1200 if wT is None else wT
        tmp_ustar =  ustar if ustar is not None else 0.1      if uw is None else np.sqrt(np.abs(uw))
        
        
        # append new particles to arrays of existing particles
        self.xo = np.append(self.xo, newx)
        self.yo = np.append(self.yo, newy)
        self.zo = np.append(self.zo, newz)
        self.x = np.append(self.x, newx)
        self.y = np.append(self.y, newy)
        self.z = np.append(self.z, newz)
        self.up = np.append(self.up, np.ones(count) * up)
        self.vp = np.append(self.vp, np.ones(count) * vp)
        self.wp = np.append(self.wp, np.ones(count) * wp)
        self.ustar = np.append(self.ustar, np.ones(count) * tmp_ustar)
        self.theta = np.append(self.theta, np.ones(count) * theta)
        self.H = np.append(self.H, np.ones(count) * tmp_H)
        self.flag =  np.append(self.flag, np.ones(count) * flag)
            
    def add_particle_vector(self, count, x,y,z, azim, elev, rang, ustar=None,  uw=None,  H=None,  wT=None, theta=0, flag=0):
        #  This function adds a vector of particles to the set of all particles used in the diffusion calculations
        #
        #  count is the number of particles to add if count is negative they will be randomly distributed, otherwise they will be uniformly distributed
        #
        #  x,y,z are the position of the vector souurce
        #  azim, elev, rang are the direciton and distannce of the vector relative to the source postion
        #                       
        #
        #  ustar and uw are friction velocity and uw covariance of the surface.  If ustar is not given then it 
        #       will be calculated from uw
        #
        #  H and wT are sensible heat flux and wT covariance of the surface.  If H is not given then it 
        #       will be calculated from wT
        #
        #   theta is the low frequency wind direction in degrees
        #
        #   flag is a label attached to the group of particles created.
        #

        newx, newy, newz = self._give_new_source_particle_vector( x, y, z,  azim, elev, rang, count)
        count = np.abs(count)
        
        # convert low freq wind direction to radians
        theta = np.deg2rad(theta)
        
        # get ustar and w'T' values
        tmp_H    =  H/1200 if H     is not None else 100/1200 if wT is None else wT
        tmp_ustar =  ustar if ustar is not None else 0.1      if uw is None else np.sqrt(np.abs(uw))
        
        
        # append new particles to arrays of existing particles
        self.xo = np.append(self.xo, newx)
        self.yo = np.append(self.yo, newy)
        self.zo = np.append(self.zo, newz)
        self.x = np.append(self.x, newx)
        self.y = np.append(self.y, newy)
        self.z = np.append(self.z, newz)
        self.up = np.append(self.up, np.zeros(count))
        self.vp = np.append(self.vp, np.zeros(count))
        self.wp = np.append(self.wp, np.zeros(count))
        self.ustar = np.append(self.ustar, np.ones(count) * tmp_ustar)
        self.theta = np.append(self.theta, np.ones(count) * theta)
        self.H = np.append(self.H, np.ones(count) * tmp_H)
        self.flag =  np.append(self.flag, np.ones(count) * flag)
            
        
    def _limit_to_range(self):
        dist = np.sqrt(self.x**2 + self.y**2)
        
        # tally particles outside domain
        idout = dist >= self.range
        if np.sum(idout)>0:
            direct = np.round(np.rad2deg(np.arctan2(self.y[idout], self.x[idout]))).astype(int)
            direct[direct<0] = direct[direct<0] + 360
            height = np.round(self.z[idout]*300/self.max_height).astype(int)
            height[height > 299] = 299
            height[height < 0] = 0
            np.add.at(self.exit_count, (direct,height) , 1)
        
        # eliminate particles outside domain
        idin = dist < self.range
        self.xo = self.xo[idin]
        self.yo = self.yo[idin]
        self.zo = self.zo[idin]
        self.x = self.x[idin]
        self.y = self.y[idin]
        self.z = self.z[idin]
        self.up =self.up[idin]
        self.vp = self.vp[idin]
        self.wp = self.wp[idin]
        self.ustar = self.ustar[idin]
        self.theta = self.theta[idin]
        self.H = self.H[idin]
        self.flag = self.flag[idin]

    def _get_touchdowns_and_bounce(self):
        # get touchdowns
        idtd = self.z <= (self.disp_height + self.roughness)
        self.tds = self.tds + np.sum(idtd)
        tmpx = (self.x[idtd]+self.range) *  401/(1+2*self.range)
        tmpy = (self.y[idtd]+self.range) *  401/(1+2*self.range)
        tmpx[tmpx<0] = 0
        tmpy[tmpy<0] = 0
        tmpx[tmpx>400] = 400
        tmpy[tmpy>400] = 400
        tmpx = tmpx.astype(int)
        tmpy = tmpy.astype(int)
        np.add.at(self.footprint_count, (tmpx,tmpy) , 1)
        self.z[idtd] =  2*(self.disp_height + self.roughness) - self.z[idtd] 

    def _calc_sigmas(self, L, zmd, idu, idn, ids):        
        sigma_u = np.zeros(len(self.x))
        sigma_v = np.zeros(len(self.x))
        sigma_w = np.zeros(len(self.x))
        dsw2dz = np.zeros(len(self.x))
        dswdz = np.zeros(len(self.x))
        
        sigma_u[ids]  = 2.0*self.ustar[ids]
        sigma_v[ids]  = 1.4*self.ustar[ids]
        sigma_w[ids]  = 1.25*self.ustar[ids]

        sigma_u[idn]  = 2.0*self.ustar[idn]
        sigma_v[idn]  = 1.4*self.ustar[idn]
        sigma_w[idn]  = 1.25*self.ustar[idn]
        
        sigma_u[idu] = np.sqrt(0.35*self.ustar[idu]**2*((self.BL_height/(0.4*L[idu]))**2)**(1/3) + 2.0*self.ustar[idu]**2)
        sigma_v[idu] = sigma_u[idu]
        sigma_w[idu] = 1.3*self.ustar[idu] * (1.0 - 3*zmd[idu]/L[idu])**(1/3)

        dsw2dz[idu]= -13.2*self.ustar[idu]**2/(3*L[idu])*(2.2-6.6*zmd[idu]/L[idu])**-0.33
        dswdz[idu] = 4.3333*self.ustar[idu] * (9*L[idu]**-2)**(-1/3)
        
        return sigma_u, sigma_v, sigma_w, dsw2dz, dswdz 

    def _calc_timescales(self, L, zmd, idu, idn, ids): 
        
        T_u = np.zeros(len(self.x))
        T_v = np.zeros(len(self.x))
        T_w = np.zeros(len(self.x))
        
        # Lagrangian  time scales from Liz's thesis
#        T_u  = 0.5*zmd/sigma_u
#        T_v  = 0.5*zmd/sigma_v
#        T_w  = 0.5*zmd/sigma_w       
#        
#        T_u[ids]    = T_u[ids]*(1+5*zmd[ids]/L[ids])**-1 
#        T_v[ids]    = T_v[ids]*(1+5*zmd[ids]/L[ids])**-1 
#        T_w[ids]    = T_w[ids]*(1+5*zmd[ids]/L[ids])**-1 
#        
#        T_u[idu]    = T_u[idu] *(1-6*zmd[idu]/L[idu])**0.25 
#        T_v[idu]    = T_v[idu] *(1-6*zmd[idu]/L[idu])**0.25 
#        T_w[idu]    = T_w[idu] *(1-6*zmd[idu]/L[idu])**0.25 
        

        # Lagrangian  time scales from Java code
        T_u  =  0.24*zmd/self.ustar
        T_v  =  0.24*zmd/self.ustar
        T_w  =  0.24*zmd/self.ustar     
        
        T_u[ids]    = 0.2*zmd[ids]/(self.ustar[ids]*(1+4*zmd[ids]/L[ids]))
        T_v[ids]    = 0.2*zmd[ids]/(self.ustar[ids]*(1+4*zmd[ids]/L[ids]))
        T_w[ids]    = 0.2*zmd[ids]/(self.ustar[ids]*(1+4*zmd[ids]/L[ids])) 
        
        T_u[idu]    = 0.2*zmd[idu]/(self.ustar[idu]*(1-6*zmd[idu]/L[idu])**0.25) 
        T_v[idu]    = 0.2*zmd[idu]/(self.ustar[idu]*(1-6*zmd[idu]/L[idu])**0.25) 
        T_w[idu]    = 0.2*zmd[idu]/(self.ustar[idu]*(1-6*zmd[idu]/L[idu])**0.25) 

        return T_u, T_v, T_w

    def _calc_windspeeds(self, L, zmd, idu, idn, ids): 
        psi = np.zeros(len(self.x))
        psi_0 = np.zeros(len(self.x))

        x1 = (1-15*zmd[idu]/L[idu])**0.25
        psi[idu]   = 2*np.log( (1+x1)/2) + np.log( (1+x1**2)/2) - 2*np.arctan(x1)+np.pi/2
        psi[idn] = 0
        psi[ids] = -4.7*zmd[ids]/L[ids]

        x2 = (1-15*self.roughness/L[idu])**0.25
        psi_0[idu] = 2*np.log( (1+x2)/2) + np.log( (1+x2**2)/2) - 2*np.arctan(x2)+np.pi/2
        psi_0[idn] = 0
        psi_0[ids] = -4.7*self.roughness/L[ids]

        Uh = (self.ustar/0.4) * (np.log ( zmd/ self.roughness ) - (psi - psi_0))
    
        return Uh
    
    
    def update_particles(self, dt):
        
        L = (-1*self.Tv * self.ustar**3)/(0.4*9.83 * self.H) 
        zmd = self.z - self.disp_height
        
        
        idn = np.abs(L) > 99.0
        ids = (L >= 0.0) & (L <= 99.0)
        idu = (L < 0.0) & (L >= -99.0)
       
        
        # calc standard deviations
        sigma_u, sigma_v, sigma_w, dsw2dz, dwswdz = self._calc_sigmas( L, zmd, idu, idn, ids)    
        
        # calc time scales
        T_u, T_v, T_w =  self._calc_timescales( L, zmd, idu, idn, ids)
        
        # wind speed extracted  from stability adjusted profile
        Uh = self._calc_windspeeds( L, zmd, idu, idn, ids)
        # calc mean horizontal wind vector components 
        uh = Uh*np.cos(self.theta)
        vh = Uh*np.sin(self.theta)
        
        # calculate the random walk/markov chain fluctuating velocity components
        self.up = self.up*(1-dt/T_u)+(2*dt*sigma_u**2/T_u)**0.5* np.random.randn(len(sigma_u))
        self.vp = self.vp*(1-dt/T_v)+(2*dt*sigma_v**2/T_v)**0.5* np.random.randn(len(sigma_v))
        # wp from Liz's thesis
        self.wp = self.wp + dt*(-1*self.wp/T_w + 0.5*(1+(self.wp/sigma_w)**2)*dsw2dz) + np.sqrt(2*dt*sigma_w**2/T_w)* np.random.randn(len(sigma_w))
        # wp from NAME model
        #self.wp = self.wp*(1-dt/T_w) + np.random.randn(len(sigma_w))*np.sqrt(2*dt*sigma_w**2/T_w)  + dt*dswdz*(sigma_w**2 + self.wp**2)/sigma_w 
        

        # advect the particle by the mean + fluctuation over the time step
        self.x = self.x + dt*(uh+self.up) 
        self.y = self.y + dt*(vh+self.vp) 
        self.z = self.z + dt*self.wp

        # tally up the touch downs  ( for backward lagranian footprints)
        self._get_touchdowns_and_bounce()
        
        # tally then eliminate particles outwith the measurement domain
        self._limit_to_range()

        # measure detectors (i.e. for forward lagrangian footprints) 
        for adetector in self.detectors:
            adetector.measure(self.x, self.y, self.z, self.xo, self.yo, self.zo, self.H, self.ustar)
