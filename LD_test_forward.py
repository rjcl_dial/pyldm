# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 12:33:15 2017

@author: rclement
"""
import numpy as np
import pyLDM.LDMmodel as LDM     
from matplotlib import pyplot as plt


# create model
hrng = 500
vrng = 100
mod = LDM.diffuse(1000,0,0.05,280,hrng,vrng)  

mod.add_detector(0,0,10,2)

#fig = plt.figure()
#plt.ion()
#ax1 = plt.subplot(111)

# add initial particles

mod.add_particles(10000,[-500, 0],[-300, 300],[0.15],ustar=0.3,H=300, theta=0)

#loop over time (0.1 sec time step)
for ix in range(20000):   
    # update particles
    mod.update_particles(0.1)

    if np.mod(ix,1000) == 0:
#        ax1.cla()
#        ax1.scatter(mod.x,mod.y, s=1, c=mod.z)
#        ax1.set_xlim(-1*hrng,hrng)
#        ax1.set_ylim(-1*hrng,hrng)
#        plt.pause(0.01)
        print(ix)
        mod.add_particles(10000,[-500, 0],[-300, 300],[0.15],ustar=0.3,H=300, theta=0)


adetect = mod.detectors[0]
plt.figure()
plt.scatter(adetect.source_x, adetect.source_y)
plt.xlim(-1*hrng,hrng)
plt.ylim(-1*hrng,hrng)
plt.show()

